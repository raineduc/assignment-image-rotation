#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>
#include "file_io.h"
#include "image.h"
#include "rotate.h"
#include "util.h"
#include "bmp.h"

static const char* const write_error_messages[] = {
        [WRITE_OK] = "Done",
        [WRITE_ERROR] = "Failed to write data to file",
        [WRITE_SOURCE_ACCESS_FAILED] = "Could not access the file"
};

static const char* const read_error_messages[] = {
        [READ_OK] = "Done",
        [READ_INVALID_BITS] = "Failed to read pixels: invalid bits",
        [READ_INVALID_HEADER] = "Invalid file header",
        [READ_INVALID_SIGNATURE] = "Invalid signature",
        [READ_SOURCE_ACCESS_FAILED] = "Could not access the file"
};

int main(int argc, char* argv[]) {
    if (argc != 3) err("Format of the command: rotate *source image* *path to new image*\n");
    struct image image = { 0 };
    enum read_status r_status = read_image(argv[1], &image, from_bmp);
    if (r_status != READ_OK) err(read_error_messages[r_status]);
    struct image rotated = rotate(image);
    enum write_status w_status = write_image(argv[2], &rotated, to_bmp);
    if (w_status != WRITE_OK) err(write_error_messages[w_status]);
    return 0;
}
