#ifndef IMAGE_ROTATION_BMP_H
#define IMAGE_ROTATION_BMP_H

#include <stdint.h>
#include "image.h"

enum read_status from_bmp(FILE* in, struct image* image);
enum write_status to_bmp(FILE* out, struct image* image);

#endif //IMAGE_ROTATION_BMP_H
