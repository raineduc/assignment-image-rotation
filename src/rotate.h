#ifndef IMAGE_ROTATION_ROTATE_H
#define IMAGE_ROTATION_ROTATE_H

#include "image.h"

struct image rotate(struct image);

#endif //IMAGE_ROTATION_ROTATE_H
