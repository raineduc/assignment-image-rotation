#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t, uint64_t, struct pixel*);

size_t get_pixel_index_from_coords(uint64_t x, uint64_t y, uint64_t imageWidth);

void destroy_image(struct image);

#endif //IMAGE_ROTATION_IMAGE_H
