#ifndef IMAGE_ROTATION_FILE_IO_H
#define IMAGE_ROTATION_FILE_IO_H

#include <stdio.h>
#include "image.h"

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_SOURCE_ACCESS_FAILED,
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_SOURCE_ACCESS_FAILED,
};

typedef enum read_status (*to_image_data) (FILE*, struct image*);
typedef enum write_status (*to_file) (FILE*, struct image*);

enum read_status read_image(const char* filename, struct image* image, to_image_data to_image);
enum write_status write_image(const char* filename, struct image* image, to_file write_to_file);

#endif //IMAGE_ROTATION_FILE_IO_H
