#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "bmp.h"
#include "file_io.h"

#define BMP_TYPE 0x4D42
#define PIXEL_SIZE 3

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static size_t calc_padding(uint32_t imageWidth) {
    return (4 - PIXEL_SIZE * imageWidth % 4) % 4;
}

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof(struct bmp_header), 1, f );
}

static bool write_header(FILE *f, struct bmp_header *header) {
    return fwrite(header, sizeof(struct bmp_header), 1, f);
}


static struct bmp_header create_bmp_header(uint32_t width, uint32_t height) {
    uint32_t image_size = width * height * 3 + calc_padding(width) * height;
    return (struct bmp_header) {
            .bfType = BMP_TYPE,
            .bfileSize = sizeof(struct bmp_header) + image_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum read_status validate_header(FILE* in, struct bmp_header* header) {
    if (!read_header(in, header)) return READ_INVALID_HEADER;
    if (header->bfType != BMP_TYPE) return READ_INVALID_SIGNATURE;
    if ((header->bOffBits - ftell(in) > 0)) {
        if (fseek(in, header->bOffBits - ftell(in), 1)) return READ_INVALID_HEADER;
    }
    return READ_OK;
}


enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header header = {};

    const enum read_status header_read_status = validate_header(in, &header);

    if (header_read_status != READ_OK) {
        return header_read_status;
    }

    const size_t padding = calc_padding(header.biWidth);
    struct pixel* pixels = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
    for (uint32_t row = 0; row < header.biHeight; row++) {
        const size_t index = get_pixel_index_from_coords(0, row, header.biWidth);
        const int32_t count = fread(&pixels[index], sizeof(struct pixel), header.biWidth, in);

        if (count != header.biWidth || fseek(in, padding, 1)) {
            free(pixels);
            return READ_INVALID_BITS;
        }
    }
    *image = (struct image) { .width = header.biWidth, .height = header.biHeight, .data = pixels };
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image* image) {
    struct bmp_header header = create_bmp_header(image->width, image->height);
    if (!write_header(out, &header)) return WRITE_ERROR;
    const size_t padding = calc_padding(image->width);
    const int32_t padding_value = 0;
    for (uint64_t row = 0; row < image->height; row++) {
        const size_t index = get_pixel_index_from_coords(0, row, image->width);
        const int32_t count = fwrite(&image->data[index], sizeof(struct pixel), image->width, out);
        if (count != image->width || fwrite(&padding_value, 1, padding, out) != padding) {
            destroy_image(*image);
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
