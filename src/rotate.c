#include <stdlib.h>
#include <stdint.h>
#include "image.h"

uint64_t rotate_x_90(uint64_t y, uint64_t imageHeight) {
    // rotate about (0,0) in cartesian system (newX = -y);
    // revert back to matrix representation(newX = -y + height);
    return -y + (imageHeight - 1);
}

uint64_t rotate_y_90(uint64_t x) {
    return x;
}

struct image rotate(struct image const source) {
    struct pixel* pixels = malloc(sizeof(struct pixel) * source.width * source.height);
    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            const size_t index = get_pixel_index_from_coords(x, y, source.width);
            const uint64_t newX = rotate_x_90(y, source.height);
            const uint64_t newY = rotate_y_90(x);
            const size_t new_index = get_pixel_index_from_coords(newX, newY, source.height);
            pixels[new_index] = source.data[index];
        }
    }
    return create_image(source.height, source.width, pixels);
}
