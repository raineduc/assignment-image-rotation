#include <stdint.h>
#include <stdlib.h>
#include "image.h"

struct image create_image(uint64_t width, uint64_t height, struct pixel* data) {
    return (struct image) { .width = width, .height = height, .data = data };
}

size_t get_pixel_index_from_coords(uint64_t x, uint64_t y, uint64_t imgWidth) {
    return y * imgWidth + x;
}

void destroy_image(struct image img) {
    free(img.data);
}

