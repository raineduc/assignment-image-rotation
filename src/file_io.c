#include <stdio.h>
#include "file_io.h"

enum read_status read_image(const char* filename, struct image* image, to_image_data to_image) {
    FILE* file = fopen(filename, "r");
    if (!file) return READ_SOURCE_ACCESS_FAILED;
    const enum read_status status = to_image(file, image);
    fclose(file);
    return status;
}

enum write_status write_image(const char* filename, struct image* image, to_file write_to_file) {
    FILE* file = fopen(filename, "w");
    if (!file) return WRITE_SOURCE_ACCESS_FAILED;
    const enum write_status status = write_to_file(file, image);
    fclose(file);
    return status;
}

